package com.brett.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.brett.model.Car;
import com.brett.util.DBUtil;

public class CarDao {

    private Connection connection;

    public CarDao() {
        connection = DBUtil.getConnection();
    }

    public void addCar(Car Car) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("insert into Cars(manufacturer,model) values (?, ? )");
            // Parameters start with 1
            preparedStatement.setString(1, Car.getManufacturer());
            preparedStatement.setInt(2, Car.getModel());
            
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteCar(int CarId) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("delete from Cars where Carid=?");
            // Parameters start with 1
            preparedStatement.setInt(1, CarId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateCar(Car Car) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("update Cars set manufacturer=?, model=?" +
                            " where Carid=?");
            // Parameters start with 1
            preparedStatement.setString(1, Car.getManufacturer());
            preparedStatement.setInt(2, Car.getModel());      
            preparedStatement.setInt(3, Car.getCarId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Car> getAllCars() {
        List<Car> Cars = new ArrayList<Car>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from Cars");
            while (rs.next()) {
                Car Car = new Car();
                Car.setCarId(rs.getInt("Carid"));
                Car.setManufacturer(rs.getString("manufacturer"));
                Car.setModel(rs.getInt("model"));
                Cars.add(Car);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Cars;
    }

    public Car getCarById(int CarId) {
        Car Car = new Car();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select * from Cars where Carid=?");
            preparedStatement.setInt(1, CarId);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                Car.setCarId(rs.getInt("Carid"));
                Car.setManufacturer(rs.getString("manufacturer"));
                Car.setModel(rs.getInt("model"));
                
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Car;
    }
}